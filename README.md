## Pipeline

- 一鍵更換 IPFS gateway 的 domain 配置:  
  - 在本 repo 的左側欄，进入 CI/CD --> Pipeline ，點擊 `Run Pipeline` 蓝色按钮，會出現輸入框，在第一行的最右边的输入框，輸入新的域名（覆盖默认值），再點擊下方 `Run Pipeline` 蓝色按钮即可。
  - 可以在 pipeline 列表页面看到 pipeline 状态（对勾or叉叉），点击进去可以看到具体的日志信息。  


## 一些协助 IPFS 的脚本

### IN USE

- ipfs-gateway-pin.sh: (cron) 在 Gateway 上定時 pin 最新的 hash (CID)。  

- ipfs-pin.sh: 在 IPFS daemon server，add/pin 網站內容，並刪除舊的 CID (via pipeline)。

- pdf-convert-upload.sh: 在 IPFS daemon server，將最近新增/修改過的一篇文章轉換成 png & pdf，並上傳 IPFS (via pipeline)。  



### DEPRECATED
- `make-relative.sh`: 目前网页在其中一台机器 build 时使用了固定的 `path prefix`（dnslink），这对于 IPFS 版网页没有问题，但该服务器上的网页还需要 serve OPENNIC 域名和 Onion Service。因此仍需要切除多余的 prefix，让 prefix 变成相对的。（可能只是临时方案）    
- `sync-ipfs-gateway.sh`: （定时）对两个 IPFS gateway 发请求访问我们的每一个网页（只发请求不下载网页）—— 此举是让 gateway 总是保存我们网页的缓存，使网页在 IPFS 一直可用，并提高访问速度。  
- `matrix-bot-dnslink`: 这是个`matrix bot`插件，用于当有新的 IPFS hash 生成（即网页 build 后）时，bot 会调用 `sync-ipfs-gateway.sh`—— 加快 gateway 寻找我们网页（hash）的速度。  
- `update-dns-cloudflare.sh`, `update-dns-gandi.sh`： 当有网页 build （即 新的 hash）时，更新我们的 DNS 的 dnslink 记录 （受 Gitlab CI 调用）。  
