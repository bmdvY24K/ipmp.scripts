#!/bin/bash

## NOTE: This is used on IPFS daemon server to add/pin content.
## DATE: 2023.02.03

IPFS_PATH="/srv/go-ipfs/ipfsrepo"
# This script should be run in the git repo.
GIT_REPO="/app/krMKg1Gs/0/bmdvY24K/ipmp"

cd $GIT_REPO

CID=$(/srv/go-ipfs/ipfs pin ls --type=recursive |cut -d' ' -f1)

if [ -n "$CID" ]; then
	echo ">> Deleting old CIDs "
	/srv/go-ipfs/ipfs pin rm $CID
else
	echo ">> No old CIDs."
fi

echo ">> Pinning..."
/srv/go-ipfs/ipfs add -q -r ./public |tail -1 |tee ./public/ipfs.txt

exit
