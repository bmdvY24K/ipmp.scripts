!/bin/bash

# BRIEF         This is to curl certain gateways in order to keep some content on their servers.
# DATE    2020-04-07

URL="https://ipfs.ngocn2.org/ipfs-cid.txt"
URL0="https://ipfs.io/ipns/ipfs.ngocn2.org"
URL1="https://no-censor.website/ipns/ipfs.ngocn2.org"
URL2="https://no-censor.space/ipns/ipfs.ngocn2.org"
URL3="https://cloudflare-ipfs.com/ipns/ipfs.ngocn2.org"

LOG=/tmp/ipfs-gw-headers.log

echo "Finding the provider of CID"
CID=$(curl -s -S $URL)
/opt/go-ipfs/ipfs dht findprovs -n 2 $CID

echo "[$(date)] Getting $URL3" #|tee $LOG
#curl -L -I $URL0 >> $LOG ; echo $?  |tee -a $LOG

wget --tries=1 --timeout=5 --wait=1 --random-wait -r --spider $URL3 2>/dev/null  #|tee -a $LOG

echo "$? [$(date)] Done!" #|tee -a $LOG

sleep 2

echo "[$(date)] Getting $URL1" #|tee $LOG
#curl -L -I $URL0 >> $LOG ; echo $?  |tee -a $LOG

wget --tries=1 --timeout=5 --wait=1 --random-wait -r --spider $URL1 2>/dev/null  #|tee -a $LOG

echo "$? [$(date)] Done!" #|tee -a $LOG

# Pin the hash.
#echo "The latest CID is: $CID"
#/opt/go-ipfs/ipfs pin add  $CID ; ret=$?

exit
